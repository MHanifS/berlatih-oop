<?php

require_once("animal.php");
require_once("Ape.php");
require_once("Frog.php");


$sheep = new Animal ("shaun");
echo "Name : " . $sheep -> nama . "<br>"; 
echo "legs : " . $sheep -> legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok -> nama . "<br>"; 
echo "legs : " . $kodok -> legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
$kodok->jump();
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong -> nama . "<br>"; 
echo "legs : " . $sungokong -> legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();
echo "<br>";


?>